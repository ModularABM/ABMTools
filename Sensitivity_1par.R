#!/usr/bin/Rscript

## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## compute parameter sensitivity by varying a parameter value, perform
## simulations for each value and evaluate some function on the resulting data.
## Also plots the results.
##
## Assumes AngioABM for running simulations and geenrating output.
## 
## Author: Clemens Kuehn
## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


## libs
library(ggplot2)
library(data.table)
library(RColorBrewer)

## import Utils.R
source("../ABMTools/Utils.R")

## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## parse arguments
args <- commandArgs(TRUE)

if (args[1] == "-h" | args[1] == "--h" | length(args) < 8){
    cat("\n\nRun a simulation of an input file varying a given parameter over a sequence of vlaues. Sequence is generated using seq().\n\n")
    cat("Usage:\n\nRscript multifitABM.R xmlfile par1.name par1.start par1.end par1.num_par1.log expr use_mpi \n\n")
    cat("Where arguments are:\n")
    ## 1
    cat(paste0("\t","xmlfile1","\t--", "xml file containing WT model", "\n"))
    ## 2
    cat(paste0("\t","par1.name","\t--", "name of parameter", "\n"))
    ## 3
    cat(paste0("\t","par1.path","\t--", "path to parameter in XPATH syntax", "\n"))
    ## 4
    cat(paste0("\t","factor","\t--", "factor to change the parameter by", "\n"))
    ## 5
    cat(paste0("\t","par1.num","\t--", "length of sequence ( seq(length.out=par1.num))", "\n"))
    ## 6
    cat(paste0("\t","par1.log","\t--", "use logarithmic sequence?", "\n"))
    ## 7
    cat(paste0("\t","reps","\t--", "how many repetitions", "\n"))
    ## 8
    cat(paste0("\t","expr","\t--", "Expression to evaluate to compare model output. WARNING! This is not checked, you could do evil things here!", "\n"))
    ## 9
    cat(paste0("\t","use_mpi","\t--", "use mpi or not (== 0)", "\n"))
    ## 10
    cat(paste0("\t","sim","\t--", "simulate (== 1) or just compute on exisiting results (== 0)", "\n"))

    quit("no", 11)
}

WTname <- args[1]
par1.name <- args[2]
par1.path <- args[3]
par1.factor <- as.numeric(args[4])
par1.num <- as.numeric(args[5])
par1.log <- args[6]
reps <- args[7]
exprs <- readLines(args[8])
use_mpi <- args[9]
simswitch <- args[10]

## check exprs for being dangerous
## print("checking exprs...\n")
## parse_evalstring(exprs)
## print("exprs seems save!\n")
## construct sequence from input

exec.name <- "Angio_ABM.exe"

WT.content <- abm_readxml(WTname)
seq.line <- abm_get_entity_at_path(WT.content, par1.path, par1.name)
seq.center <- NA
if(grepl(",", seq.line)){
    ## change only the mean value, not the sd
    new.val <- strsplit(seq.line, ",")[[1]]
    seq.center <- as.numeric(new.val[2])
} else{
    seq.center <- as.numeric(seq.line)
}

as.numeric()
print(paste0("seq.center is ", seq.center))

par1.seq <- lseq(seq.center * (1 - par1.factor),
                 seq.center * (1 + par1.factor),
                 length.out = par1.num, logit = par1.log)
par1.seq <- unique(c(par1.seq, seq.center))

outname.original.WT <- abm_getoutname(WT.content)

if(simswitch == 1){
    for(i in 1:length(par1.seq)){
        cat(paste0("\n",i))
        for(j in 1:reps){
            cat(".")
            WT.new <- WT.content
            outname.new.WT <- paste0(outname.original.WT,"_",par1.name,"_",i,"_",j)
            abm_modify_path(WT.new, "//model/output", "basename", outname.new.WT)
            ## differentiate agents/locals (== sampling/not)
            if(grepl("agents", par1.path)){
                curr.val <- abm_get_entity_at_path(WT.new, par1.path, par1.name)
                if(grepl(",", curr.val)){
                    ## change only the mean value, not the sd
                    new.val <- strsplit(curr.val, ",")[[1]]
                    new.val[2] <- par1.seq[i]
                    abm_modify_path(WT.new, par1.path, par1.name,
                                    paste(new.val, collapse = ","))
                } else{
                    abm_modify_path(WT.new, par1.path, par1.name, par1.seq[i])
                }
            } else if(grepl("locals", par1.path)){
                ## for modifing svegfr1/VEGF in case modified svegfr1 diff coeff:
                ## BAD! use instead:
                ## (Dvegf^-1 + Dsvegfr1^-1)^-1 !!!
                if(par1.name == "diffusion_coeff" &
                   par1.path == "//locals/localvar[1]"){
                    newbound <- par1.seq[i] * 2 / 3
                    abm_modify_path(
                        WT.new, "//locals/localvar[2]/", "diffusion_coeff",
                        newbound )
                }
                ## Specific for modifing vegf influx:
                if(par1.name == "influx" &
                   par1.path == "//locals/localvar[3]/sources/source"){
                    newinit <- par1.seq[i] / 0.008
                    abm_modify_path(
                        WT.new, "//locals/localvar[3]/", "initvalue",
                        newinit )
                }
                abm_modify_path(WT.new, par1.path, par1.name, par1.seq[i])
            }

            WTname.new <- gsub(".xml",
                               paste0("_", par1.name, "_", i,"_",j, ".xml"),
                               WTname)
            abm_write(WT.new, WTname.new)


            ## simulate (sequential)
            if(use_mpi == 0){
                print(paste0("use_mpi is 0 and filename is ",WTname.new))
                abm_simulate(WTname.new, exec.name, "--verbose 1")
            }else{
                if(Sys.info()['sysname'] == "Linux"){
                    exec.name <- paste0("./",exec.name)
                }
                currname.WT <- WTname.new
                currjobname.WT <- paste(par1.name, i,j, sep = "_")
                sys.string.WT <- paste0("qsub -m abe -q long -l ncpus=1,nice=19",
                                        " -j oe",
                                        " -o ", currjobname.WT, ".log",
                                        " -N ",currjobname.WT,
                                        " -v directory=",getwd(),
                                        ",execname=",exec.name,
                                        ",xmlname=",currname.WT,
                                        ",jobname=",currjobname.WT,
                                        " PBSscript.pbs")
                ## submit (trying again 10 secs alter in case of failure)
                sys.ret <- system(sys.string.WT, ignore.stdout = TRUE)
                while(sys.ret > 0){
                    Sys.sleep(10)
                    sys.ret <- system(sys.string.WT, ignore.stdout = TRUE)
                }
            }
        }
    }

    if(use_mpi == 1){
        cat("waiting for simulations")
        ## check if all are finished (using qstat)
        qstate <- system(paste0("qstat -f | grep ", par1.name), intern = TRUE)
        while(length(qstate) > 0){
            cat(".")
            Sys.sleep(60)
            qstate <- system(paste0("qstat -f | grep ", par1.name), intern = TRUE)
        }
    }

    cat("\n\n\t\tALL SIMULATIONS FINISHED\n\n")
}

## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## record and plot output

results <- data.table(parval = numeric(), meanval = numeric(), sdval = numeric())
for(i in 1:length(par1.seq)){
    cat(paste0(i,","))
    res.col <- c()
    for (j in 1:reps){

        ## load simresults into datatables
        WTname.new <- gsub(".xml", paste0("_", par1.name, "_", i, "_", j, ".xml"),
                           WTname)

        simres.WT <- list()

        simres.WT <- try(abm_getsimres_verbose(abm_readxml(WTname.new)), silent = TRUE)

        if(length(simres.WT) > 1){

            idim <- max(simres.WT$locals$x)
            jdim <- max(simres.WT$locals$y)


            ## compute results
            print(paste0("will evaluate expression:\n",exprs))
        ov <- eval(parse(text = exprs))
            res.col <- c(res.col, ov)
            }
    }
    results <- rbind(results, list(par1.seq[i], mean(res.col), sd(res.col)))

}

print(results)
write.table(results, gsub(".xml",
                    paste0("_", par1.name, ".csv"),
                    WTname) ,sep = ",")


## plot results
testplot <- ggplot(data = results, aes(x = parval, y = meanval, ymax = meanval + sdval, ymin = meanval - sdval))
testplot <- testplot + geom_line(size = 2, color = "blue")
testplot <- testplot + geom_point(size = 8, color = "blue", shape = 17)
testplot <- testplot +  scale_y_continuous(exprs) +
  scale_x_continuous(par1.name) +
  theme_bw(base_size = 20) +
  ggtitle("Sensitivity") +
theme(panel.grid.major = element_line(size = 0.8))
results.center <- results[parval == seq.center,]
testplot <- testplot + geom_errorbar(size = 0.5, width = 0.01, color = "blue")
testplot <- testplot + geom_point(data = results.center, color = "red", size = 8, shape = 18)

ggsave(testplot, filename = gsub(".xml", paste0("_", par1.name, "_2.pdf"), WTname))



testplot <- ggplot(data = results, aes(x = parval, y = meanval, ymax = meanval + sdval, ymin = meanval - sdval))
testplot <- testplot + geom_ribbon(color = "blue", alpha=0.3, fill = "blue")
testplot <- testplot + geom_line(size = 2, color = "blue")
testplot <- testplot + geom_point(size = 8, color = "blue", shape = 17)
testplot <- testplot +  scale_y_continuous(exprs) +
  scale_x_continuous(par1.name) +
  theme_bw(base_size = 20) +
  ggtitle("Sensitivity") +
theme(panel.grid.major = element_line(size = 0.8))
results.center <- results[parval == seq.center,]
testplot <- testplot + geom_point(data = results.center, color = "red", size = 8, shape = 18)

ggsave(testplot, filename = gsub(".xml", paste0("_", par1.name, "_3.pdf"), WTname))
