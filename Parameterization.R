#!/usr/bin/Rscript

## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## Run small parameterization tasks: vary one parameter over a range and
## output some objective value specified in options
##
## Dangerous: reads in command from command line and eval() without any checks!
##
## Simulator is hard coded: AngioABM.exe
##
## Author: Clemens Kuehn
## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

## libs
library(ggplot2)
library(data.table)
library(RColorBrewer)
library(lhs)

## import custom libs
source("../ABMTools/Fit_Utils.R")
source("../ABMTools/Utils.R")

## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## parse arguments
args <- commandArgs(TRUE)

if (args[1] == "-h" | args[1] == "--h" | length(args) < 9){
    cat("\n\nRun a simulation of an input file varying a given parameter over a sequence of vlaues. Sequence is generated using seq().\n\n")
    cat("Usage:\n\nRscript Parameterization.R xmlfile par1.name par1.path par1.start par1.end par1.num par1.log expr use_mpi \n\n")
    cat("Where arguments are:\n")
    ## 1
    cat(paste0("\t","xmlfile","\t--", "xml file containing model", "\n"))
    ## 2
    cat(paste0("\t","par1.name","\t--", "name of parameter to vary", "\n"))
    ## 3
    cat(paste0("\t","par1.path","\t--", "path to parameter in XPATH syntax", "\n"))
    ## 4
    cat(paste0("\t","par1.start","\t--", "lowest value to use ( seq(from=par1.start))", "\n"))
    ## 5
    cat(paste0("\t","par1.end","\t--", "highest value to use ( seq(to=par1.end))", "\n"))
    ## 6
    cat(paste0("\t","par1.num","\t--", "length of sequence ( seq(length.out=par1.num))", "\n"))
    ## 7
    cat(paste0("\t","par1.log","\t--", "use logarithmic sequence?", "\n"))
    ## 8
    cat(paste0("\t","expr","\t--", "Expression to evaluate to compare model output. WARNING! This is not checked, you could do evil things here!", "\n"))
    ## 9
    cat(paste0("\t","use_mpi","\t--", "use mpi or not (== 0)", "\n"))

    quit("no", 11)
}

xmlname <- args[1]
par1.name <- args[2]
par1.path <- args[3]
par1.start <- as.numeric(args[4])
par1.end <- as.numeric(args[5])
par1.num <- as.numeric(args[6])
par1.log <- args[7]
expr <- args[8]
use_mpi <- args[9]

par1.seq <- lseq(par1.start, par1.end, length.out = par1.num, logit = par1.log)

exec.name <- "./Angio_ABM.exe"

print(paste0("xmlnamme is ",xmlname))

## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## prepare xml files and run simulations
xml.content <- abm_readxml(xmlname)
print("read xml!")
outname.original <- abm_getoutname(xml.content)


for(i in 1:length(par1.seq)){

    xml.new <- xml.content

    abm_modify_path(xml.new, par1.path, par1.name, par1.seq[i])

    outname.new <- paste0(outname.original,"_",par1.name,"_",i)

    abm_modify_path(xml.new, "//model/output", "basename", outname.new)

    xmlname.new <- gsub(".xml",
                    paste0("_", par1.name, "_", i, ".xml"),
                    xmlname)
    abm_write(xml.new, xmlname.new)

    ## simulate (sequential or using mpi)
    if(use_mpi == 0){
        print(paste0("starting sim with filename ",xmlname.new))
        system(paste(exec.name, xmlname.new))
    }else{
        currname <- xmlname.mew
        currjobname <- paste("hf", start.time, i, sep = "_")
        sys.string <- paste0("qsub -m abe -q long -l ncpus=1,nice=19",
                             " -j oe",
                             " -o ", currjobname, ".log",
                             " -N ",currjobname,
                             " -v directory=",getwd(),
                             ",execname=",exec.name,
                             ",xmlname=",currname,
                             ",jobname=",currjobname,
                             " PBSscript.pbs")
        ## submit (trying again 10 secs alter in case of failure)
        sys.ret <- system(sys.string, ignore.stdout = TRUE)
        while(sys.ret > 0){
            sys.sleep(10)
            sys.ret <- system(sys.string, ignore.stdout = TRUE)
        }
    }
}

## if use_mpi, check if all are finished (using qstat)
if(use_mpi >0){
    qstate <- system(paste0("qstat -f | grep aLI_", start.time), intern = TRUE)
    while(length(qstate) > 0){

        Sys.sleep(60)
        qstate <- system(paste0("qstat -f | grep aLI_", start.time), intern = TRUE)
    }
}
cat("\n\n\t\tALL SIMULATIONS FINISHED\n\n")


## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## record and plot output

results <- data.table(parval = numeric(), objective = numeric())
for(i in 1:length(par1.seq)){

    ## load simresults into datatables
    xmlname.curr <- gsub(".xml",
                         paste0("_", par1.name, "_", i, ".xml"),
                         xmlname)
    print(paste0("will read xml file ",xmlname.curr))
    xml.curr <- abm_readxml(xmlname.curr)
    simres <- abm_getsimres_verbose(xml.curr)

    ## compute results
    print(paste0("will evaluate expression:\n",expr))
    ov <- eval(parse(text = expr))
    results <- rbind(results, list(par1.seq[i], ov))
}

print(results)
write.table(results, gsub(".xml",
                    paste0("_", par1.name, ".csv"),
                    xmlname) ,sep = ",")
